Feature: Persistent Cart App
I want to store all the products of the customer when added to the cart

Scenario: Install persistent cart app success
When I install persistent cart app success
Then I see the persistent cart app title displayed

Scenario: Config Success when choose turn off activated config
When I choose to turn off the fixed cart 
And I click button save config
Then I see message save config success

Scenario: Display product at list cart unsuccess
When I click icon view  my website
And I click product and scroll to product list
And I click put in the cart at product want add
Then I see product at cart
When I am on List cart Page
And I do not see product at list cart

Scenario: Config Success when choose turn on activated config
Given I access config page
When I choose to turn on the fixed cart 
And I click button save config
Then I see message save config success

Scenario: Display product at list cart unsuccess when do not login account customer
When I click icon view  my website
And I click product and scroll to product list
And I click put in the cart at product want add
Then I see product at cart
When I am on List cart Page
And I do not see product at list cart

Scenario: Display product at list cart success when login account customer
When I click icon view  my website
And I turn off admin bar
And I login account customer
And I click product and scroll to product list
And I click put in the cart at product want add
Then I see product at cart
And I see product at list cart 

Scenario: Filtering unsuccess when enter the wrong email customer
When I enter the wrong email customer
And I click filtering
Then I do not see product at list cart when filtering

Scenario: Filtering success when enter the correct email customer
When I enter the correct email customer
And I click filtering
Then I see product at list cart contains email customer

Scenario: Filtering unsuccess when enter the wrong date persistent
When I enter the wrong date persistent
And I click filtering
Then I do not see product at list cart when filtering

Scenario: Filtering success when enter the correct date persistent
When I enter the correct date persistent
And I click filtering
Then I see product at list cart contains date persistent

Scenario: Filtering success when enter the correct email customer and date persistent
When I enter the correct email customer and date persistent
And I click filtering
Then I see product at list cart contains email customer and date persistent

Scenario: UnFiltering success when click UnFiltering
When I click unfiltering
Then I see product at list cart 

Scenario: View detail product at list cart success
When I click title view detail
Then I see display infomation persistent cart 

Scenario: Send notification mail out of stock success
When I click checkbox at cart want send
And I click send mail notification out of stock
Then I see message send mail notification out of stock success
And I see mail notification out of stock dislay at email customer

Scenario: I config send email notification 
Given I access config page
When I enter invalid title mail
And I click button save config
Then I see message error email
When I enter valid title mail
And I click button save config
Then I see message save config success

Scenario: Send notification mail success
When I am on List cart Page
When I click checkbox at cart want send notification
And I click send mail notification 
Then I see message send mail notification success
And I see mail notification dislay at email customer

Scenario: Reset email default success
Given I access config page
When I click button Reset email default
Then I see message notification reset email success 

Scenario: Check product at cart when logout account customer and login again
When I click icon view  my website
And I logout account customer
And I perform delete product at cart
And I login account customer
Then I see product display at cart

Scenario: Delete Persistent Cart Success
When I click checkbox at cart want delete
And I click title delete persistent cart
And I click button delete
Then I see display message delete success

Scenario: Remove app success
When I remove app success
Then I do not see settings page of app