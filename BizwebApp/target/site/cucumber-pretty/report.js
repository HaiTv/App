$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("AppLogin.feature");
formatter.feature({
  "line": 1,
  "name": "Login success",
  "description": "I want to login success",
  "id": "login-success",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 4,
  "name": "Login success",
  "description": "",
  "id": "login-success;login-success",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "I access admin site",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I login success",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I can access apps menu",
  "keyword": "Then "
});
formatter.match({
  "location": "Global.i_access_admin_site()"
});
formatter.result({
  "duration": 9006679109,
  "status": "passed"
});
formatter.match({
  "location": "Global.i_login_success()"
});
formatter.result({
  "duration": 9290809362,
  "status": "passed"
});
formatter.match({
  "location": "Global.i_can_access_apps_menu()"
});
formatter.result({
  "duration": 5387695352,
  "status": "passed"
});
formatter.uri("FavoriteProducts.feature");
formatter.feature({
  "line": 1,
  "name": "Favorite Products App",
  "description": "I want to store all the products of the customer when added to the cart",
  "id": "favorite-products-app",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 4,
  "name": "Install favorite products app success",
  "description": "",
  "id": "favorite-products-app;install-favorite-products-app-success",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "I install favorite products app success",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "I see the favorite products app title displayed",
  "keyword": "Then "
});
formatter.match({
  "location": "FavoriteProducts.i_install_favorite_products_app_success()"
});
formatter.result({
  "duration": 27666946923,
  "status": "passed"
});
formatter.match({
  "location": "FavoriteProducts.i_see_the_favorite_products_app_title_displayed()"
});
formatter.result({
  "duration": 162903449,
  "status": "passed"
});
formatter.scenario({
  "line": 8,
  "name": "Favorite products unsuccess",
  "description": "",
  "id": "favorite-products-app;favorite-products-unsuccess",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 9,
  "name": "I click icon config",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "I see config page display",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "I click setup for theme present",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "I click button accept",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "I see message setup success",
  "keyword": "Then "
});
formatter.step({
  "line": 14,
  "name": "I click icon view  my website",
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "I click product and scroll to product list",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "I click view detail product",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "I click add favorite",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "I login account customer for favorite products",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "I click list wish",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "I do not see favorite product display",
  "keyword": "Then "
});
formatter.match({
  "location": "FavoriteProducts.i_click_icon_config()"
});
formatter.result({
  "duration": 1918145211,
  "status": "passed"
});
formatter.match({
  "location": "FavoriteProducts.i_see_config_page_display()"
});
formatter.result({
  "duration": 286640585,
  "status": "passed"
});
formatter.match({
  "location": "FavoriteProducts.i_click_setup_for_theme_present()"
});
formatter.result({
  "duration": 401957203,
  "status": "passed"
});
formatter.match({
  "location": "FavoriteProducts.i_click_button_accept()"
});
formatter.result({
  "duration": 484846917,
  "status": "passed"
});
formatter.match({
  "location": "FavoriteProducts.i_see_message_setup_success()"
});
formatter.result({
  "duration": 3306124526,
  "status": "passed"
});
formatter.match({
  "location": "CompareProduct.i_click_icon_view_my_website()"
});
formatter.result({
  "duration": 281052851,
  "status": "passed"
});
formatter.match({
  "location": "CompareProduct.i_click_product_and_scroll_to_product_list()"
});
formatter.result({
  "duration": 4764353369,
  "status": "passed"
});
formatter.match({
  "location": "CompareProduct.i_click_view_detail_product()"
});
