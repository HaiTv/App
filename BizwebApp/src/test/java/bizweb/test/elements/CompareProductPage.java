package bizweb.test.elements;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CompareProductPage {
	
	@FindBy(xpath="//a[contains(.,'So sánh sản phẩm')]")
	public static WebElement titleApp;
	
	@FindBy(xpath="//*[@id='PutSnippets']")
	public static WebElement btnApplyTheme;
	
	@FindBy(xpath = "//a[contains(.,'Lưu')]")
	public static WebElement btnSave;
	
	@FindBy(xpath = "//*[@id='nav']/li[2]/a")
	public static WebElement spanProduct;
	
	@FindBy(xpath = "//div[3]/ul/li/div[1]/div[2]/div/div[2]/div[1]/div/div/label")
	public static WebElement labelAddCompareOne;
	
	@FindBy(xpath = "//div[3]/ul/li/div[2]/div[2]/div[2]/div[2]/div[1]/div/div/label")
	public static WebElement labelAddCompareTwo;
	
	@FindBy(xpath = "//div[3]/ul/li/div[3]/div[2]/div[2]/div[2]/div[1]/div/div/label")
	public static WebElement labelAddCompareThree;
	
	@FindBy(xpath = "//div[3]/ul/li/div[4]/div[2]/div[2]/div[2]/div[1]/div/div/label")
	public static WebElement labelAddCompareFour;

	@FindBy(xpath = "//div[3]/ul/li/div[5]/div[2]/div[2]/div[2]/div[1]/div/div/label")
	public static WebElement labelAddCompareFive;
	
	@FindBy(xpath = "//input[@type='checkbox']")
	public static WebElement checkboxLogitech;
	
	@FindBy(xpath = "//*[@id='jGrowl-product-compare']/div/div[2]/div[2]/table/tbody/tr/td[2]/p/b")
	public static WebElement titleAddSuccess;
	
	@FindBy(xpath = "//*[@id='product-compare-button']")
	public static WebElement btnCompareProduct;
	
	@FindBy(xpath = "//*[@id='product-compare-list']/div[1]/div[1]/div[2]/div[1][@class='product-compare-name']")
	public static WebElement titleNameProductInFrame;
	
	@FindBy(xpath = "//*[@id='product-compare-list']/div[2]/div[1]/div[2]/div[1][@class='product-compare-name']")
	public static WebElement titleNameProductTwoInFrame;
	
	@FindBy(xpath = "//*[@id='product-compare-list']/div[3]/div[1]/div[2]/div[1][@class='product-compare-name']")
	public static WebElement titleNameProductThreeInFrame;
	
	@FindBy(xpath = "//*[@id='product-compare-list']/div[4]/div[1]/div[2]/div[1][@class='product-compare-name']")
	public static WebElement titleNameProductFourInFrame;
	
	@FindBy(xpath = "//*[@id='product-compare-frame']/div[2]/a")
	public static WebElement btnCompareInFrame;
	
	@FindBy(xpath = "//h2[contains(.,'So sánh sản phẩm')]")
	public static WebElement titleCompareProductPage;
	
	@FindBy(xpath = "//*[@id='product-compare-contents']/h4")
	public static WebElement titleCompareUnSuccess;
	
	@FindBy(xpath = "//*[@id='product-compare-contents']/table/tbody/tr[1]/th")
	public static WebElement titleCompareSuccess;
	
	@FindBy(xpath = "//*[@id='product-compare-list']/div/div[1]/button")
	public static WebElement btnDeleteProduct;
	
	@FindBy(xpath = "//*[@id='product-compare-contents']/table/tbody/tr[7]/td[1]/input")
	public static WebElement btnEliminateProduct;
	
	// Config Page
	
	@FindBy(xpath = "//span[@class='title']")
	public static WebElement spanConfig;
	
	@FindBy(xpath = "//input[@value='2']")
	public static WebElement checkboxIconCompare;
	
	@FindBy(xpath = "//input[@value='1']")
	public static WebElement checkboxTabCompare;
	
	@FindBy(xpath = "//*[@id='ButtonSetting_AddButtonName']")
	public static WebElement txtButtonAddCompareProduct;
	
	@FindBy(xpath = "//*[@id='ButtonSetting_AddButtonName-error']")
	public static WebElement titleErrorEnterName;
	
	// View detail
	
	@FindBy(xpath = "//div[3]/ul/li/div[1]/div[2]/div/div[1]/div")
	public static WebElement btnViewdetail;
	
	@FindBy(xpath = "//div/div[2]/div[4]/div[2]/input")
	public static WebElement btnAddCompare;
	
}
