package bizweb.test.elements;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage {
	//Login
			@FindBy(xpath="//input[@id='Email']")
			public static WebElement email;
			
			@FindBy(xpath="//input[@id='Password']")
			public static WebElement password;
			
			@FindBy(xpath="//input[@value='Đăng nhập']")
			public static WebElement loginBtn;
			
			@FindBy(xpath="//a[@href='/admin/apps']")
			public static WebElement menuApps;
			
			@FindBy(xpath="//span[@class='title']")
			public static WebElement titlePage;
			
		// Logout
			@FindBy(xpath="//div[@class='account-info']")
			public static WebElement account_info;
			
			@FindBy(xpath="//a[@href='/admin/authorization/logout']")
			public static WebElement logout;
}
