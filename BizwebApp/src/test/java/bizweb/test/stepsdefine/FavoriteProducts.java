package bizweb.test.stepsdefine;

import org.openqa.selenium.support.PageFactory;

import bizweb.test.actions.Base;
import bizweb.test.elements.BizwebFormPage;
import bizweb.test.elements.CloudStoragePage;
import bizweb.test.elements.FavoriteProductsPage;
import bizweb.test.elements.PersistentCartPage;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class FavoriteProducts extends Base{
	
	public FavoriteProducts() {
		PageFactory.initElements(driver, FavoriteProductsPage.class);
		PageFactory.initElements(driver, BizwebFormPage.class);
		PageFactory.initElements(driver, CloudStoragePage.class);
		PageFactory.initElements(driver, PersistentCartPage.class);
	}
	
	@When("^I install favorite products app success$")
	public void i_install_favorite_products_app_success() throws Throwable {
	   installApp("Sản phẩm yêu thích");
	}

	@Then("^I see the favorite products app title displayed$")
	public void i_see_the_favorite_products_app_title_displayed() throws Throwable {
		waitElementWithText(FavoriteProductsPage.titleApp, "Sản phẩm yêu thích");
	}
	
	@When("^I click icon config$")
	public void i_click_icon_config() throws Throwable {
		waitElement(BizwebFormPage.iframe);
	    switchToIframe(BizwebFormPage.iframe);
	    waitElementToClick(FavoriteProductsPage.btnConfig);
	    exitIframe();
	}

	@Then("^I see config page display$")
	public void i_see_config_page_display() throws Throwable {
	    waitElementWithText(FavoriteProductsPage.titleConfig, "Cấu hình");
	}

	@When("^I click setup for theme present$")
	public void i_click_setup_for_theme_present() throws Throwable {
		waitElement(BizwebFormPage.iframe);
	    switchToIframe(BizwebFormPage.iframe);
	    waitElementToClick(FavoriteProductsPage.btnSetupThemePresent);
	}

	@When("^I click button accept$")
	public void i_click_button_accept() throws Throwable {
		waitElementToClick(CloudStoragePage.btnAccept);
	    exitIframe();
	}

	@Then("^I see message setup success$")
	public void i_see_message_setup_success() throws Throwable {
	    waitElementWithText(BizwebFormPage.titleNotification, "Cài đặt thành công");
	}

	@When("^I click add favorite$")
	public void i_click_add_favorite() throws Throwable {
	    waitElement(FavoriteProductsPage.btnAddFavorite);
	    clickToPoint(FavoriteProductsPage.btnAddFavorite);
	}

	@When("^I login account customer for favorite products$")
	public void i_login_account_customer_for_favorite_products() throws Throwable {
		waitElementToSendKeys(PersistentCartPage.txtEmailFrontend, "haibiboy1995@gmail.com");
		waitElementToSendKeys(PersistentCartPage.txtPassFrontend, "haibib0y1995");
		waitElementToClick(PersistentCartPage.btnLoginFrontend);
	}

	@When("^I click list wish$")
	public void i_click_list_wish() throws Throwable {
	    waitElementToClick(FavoriteProductsPage.btnWishList);
	}

	@Then("^I do not see favorite product display$")
	public void i_do_not_see_favorite_product_display() throws Throwable {
	    
	}
	
	@Then("^I see the product has favorite$")
	public void i_see_the_product_has_favorite() throws Throwable {
	    waitElementWithText(FavoriteProductsPage.btnHasFavorite, "Đã yêu thích");
	}

	@Then("^I see favorite product display at list wish$")
	public void i_see_favorite_product_display_at_list_wish() throws Throwable {
	    waitElementWithText(FavoriteProductsPage.titleProductAtWishList, "iPad Wifi 3G 16GB");
	    closeTabWindow(2);
	}
	
	@When("^I click button comeback$")
	public void i_click_button_Comeback() throws Throwable {
		switchTabWindow(1);
		waitElementToClick(BizwebFormPage.btnComeBack);
	}
	
	@When("^I access manager product page$")
	public void i_access_manager_product_page() throws Throwable {
	    waitElement(BizwebFormPage.iframe);
	    switchToIframe(BizwebFormPage.iframe);
	    waitElementToClick(FavoriteProductsPage.btnManagerProduct);
	}

	@Then("^I see favorite product display at manager product page$")
	public void i_see_favorite_product_display_at_manager_product_page() throws Throwable {
	    waitElementWithText(FavoriteProductsPage.titleProductAtManagerProduct, "iPad Wifi 3G 16GB");
	}
}
