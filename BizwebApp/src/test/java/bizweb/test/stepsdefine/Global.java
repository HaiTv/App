package bizweb.test.stepsdefine;

import org.openqa.selenium.support.PageFactory;

import bizweb.test.actions.Base;
import bizweb.test.actions.Const;
import bizweb.test.elements.GlobalPage;
import bizweb.test.elements.LoginPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Global extends Base{
	public Global(){
		PageFactory.initElements(driver, GlobalPage.class);
		PageFactory.initElements(driver, LoginPage.class);
	}
	
	//Login
	@Given("^I access admin site$")
	public void i_access_admin_site() throws Exception {
		open(Const.URL_ADMIN);
	}

	@When("^I login success$")
	public void i_login_success() throws Exception {
		login(Const.EMAIL, Const.PASS);
	}

	@Then("^I can access apps menu$")
	public void i_can_access_apps_menu() throws Exception {
		waitElementToClick(LoginPage.menuApps);
		waitElementWithText(LoginPage.titlePage, "Ứng dụng");
	}
	
	// Install
	@Then("^I see Settings page of app$")
	public void i_see_Setting_page_of_app() throws Exception {
		Thread.sleep(3000);
		assertText(GlobalPage.titlePage, "Cấu hình");
	}
	
	//Uninstall app
	@When("^I remove app$")
	public void i_remove_app() throws Exception {
		unInstallApp();
	}

	@Then("^I do not see app in list app$")
	public void i_do_not_see_app_in_list_app() throws Exception {
		waitElementWithText(GlobalPage.contentPage, "Website của bạn chưa cài ứng dụng nào");
	}
}
