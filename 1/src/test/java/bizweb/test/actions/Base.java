package bizweb.test.actions;

import java.util.ArrayList;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.support.ui.ExpectedConditions;

import bizweb.test.elements.GlobalPage;
import bizweb.test.elements.LoginPage;

public class Base extends WebDriverAction{
	public void login(String email, String pass){
		waitElement(LoginPage.email);
		sendKeys(LoginPage.email, email);
		sendKeys(LoginPage.password, pass);
		click(LoginPage.loginBtn);
	}
	
	public void installApp(String appName) throws InterruptedException{
		click(GlobalPage.btnAppInventory);
		Thread.sleep(2000);
		switchTabWindow(1);
		waitElement(GlobalPage.txtSearch);
		scroll(0,300);
		sendKeys(GlobalPage.txtSearch, appName);
		click(GlobalPage.btnSearch);
		Thread.sleep(2000);
		waitElement(GlobalPage.itemApp);
		scroll(0,300);
		click(GlobalPage.itemApp);
		waitElementToClick(GlobalPage.btnSetup);
		waitElementToClick(GlobalPage.btnLogin);
		waitElementToClick(GlobalPage.btnInstall);
	}
	
	public void unInstallApp() throws InterruptedException{
		// close multil tab window
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		try{
			for(int index = 1; index <= tabs.size()-1; index++)
			{
				driver.switchTo().window(tabs.get(index));
				driver.close();
			}
		}
		catch(NoSuchWindowException e) {
			System.out.println("Tabs was closed!");
			   }
		switchTabWindow(0);
		click(LoginPage.menuApps);
		waitElementToClick(GlobalPage.btnMore);
		waitElementToClick(GlobalPage.deleteApp);
		waitElementToClick(GlobalPage.btnDelete);
	}
	
	public void logout(){
		waitElementToClick(LoginPage.account_info);
		waitElementToClick(LoginPage.logout);
//		Assert.assertEquals("Đăng nhập quản trị hệ thống", loginPage.titleLogin.getText());		
	}
	
	public void loginGmail(String email, String pass){
		waitElement(GlobalPage.emailGoogle);
		sendKeys(GlobalPage.emailGoogle, email);
		click(GlobalPage.btnNext);
		waitElement(GlobalPage.passGoogle);
		sendKeys(GlobalPage.passGoogle, pass);
		click(GlobalPage.btnNext);
		wait.until(ExpectedConditions.titleContains("Hộp thư đến"));
	}
	
	public void waitNotification(String locator, String text){
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(locator)));
	    Assert.assertEquals(text, driver.findElement(By.xpath(locator)).getText());
	}	
	

	public void createNewProduct(String text, String price){
		waitElementToClick(GlobalPage.menuProduct);
		waitElementToClick(GlobalPage.btnCreateNewProduct);
		waitElementToClick(GlobalPage.txtNameProduct);
		sendKeys(GlobalPage.txtNameProduct, text);
		scroll(0, 650);
		driver.findElement(By.id("//a[@bind-event-click='openFileUpload()']")).sendKeys("/src/test/resources/Image/1.jpg");
		clear(GlobalPage.txtPriceProduct);
		sendKeys(GlobalPage.txtPriceProduct, price);
		click(GlobalPage.btnSaveProduct);
		
	}
	public void deleteOrder(String numberOrder){
		click(GlobalPage.menuOrder);
		sendKeys(GlobalPage.textSearchOrder, numberOrder);
		GlobalPage.textSearchOrder.sendKeys(Keys.ENTER);
		click(GlobalPage.linkOrderDelete);
		click(GlobalPage.btnCancelOrder);
		click(GlobalPage.btnCancelOrderPopup);
		click(GlobalPage.btnDeleteOrder);

	}
}
