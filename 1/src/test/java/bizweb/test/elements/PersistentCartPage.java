package bizweb.test.elements;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PersistentCartPage {
	
	@FindBy(xpath="//a[contains(.,'Lưu trữ giỏ hàng')]")
	public static WebElement titleApp;
	
	@FindBy(xpath="//*[@id='Enable']")
	public static WebElement selectboxActivated;
	
	@FindBy(xpath="//div/div[2]/div//a[contains(.,'Lưu')]")
	public static WebElement btnSave;
	
	@FindBy(xpath="//div[1]/div[1]/div[2]/div[3]/form/div/button[@title='Cho vào giỏ hàng']")
	public static WebElement buttonAddCart;
	
	@FindBy(xpath="//*[@id='popup-cart-desktop']/div[1]")
	public static WebElement titleAddCartSuccess;
	
	@FindBy(xpath="//a[contains(.,'Danh sách giỏ hàng')]")
	public static WebElement btnListCart;
	
	@FindBy(xpath="html/body/div[1]/div/div/div/div[2]/h3")
	public static WebElement titleDontProduct;
	
	@FindBy(xpath="//*[@id='content']/div/div[2]/div/a[contains(.,'Cấu hình')]")
	public static WebElement btnConfig;
	
	@FindBy(xpath="//a[@href='/account/login']")
	public static WebElement titleLoginFrontend;
	
	@FindBy(xpath="//*[@id='customer_email']")
	public static WebElement txtEmailFrontend;
	
	@FindBy(xpath="//*[@id='customer_password']")
	public static WebElement txtPassFrontend;
	
	@FindBy(xpath="//input[@value='Đăng nhập']")
	public static WebElement btnLoginFrontend;
	
	@FindBy(xpath="//*[@id='customer_login_link']")
	public static WebElement btnLogin;
	
	@FindBy(xpath="html/body/div[1]/div/div/div/div[2]/table/thead/tr/th[3]")
	public static WebElement titleProduct;
	
	@FindBy(xpath="//a[contains(.,'Chi tiết')]")
	public static WebElement titleViewDetail;
	
	@FindBy(xpath="//h4[contains(.,'Thông tin lưu trữ giỏ hàng')]")
	public static WebElement titleInfomationPersistentCart;

	
	@FindBy(xpath="//*[@id='CompareModal']/div/div/div[1]/button")
	public static WebElement btnCloseViewDetail;
	
	@FindBy(xpath="//*[@id='queryEmail']")
	public static WebElement txtEmail;
	
	@FindBy(xpath="//*[@id='dateCart']")
	public static WebElement txtDate;
	
	@FindBy(xpath="//button[1][contains(.,'Lọc')]")
	public static WebElement btnFiltering;
	
	@FindBy(xpath="//button[contains(.,'Bỏ Lọc')]")
	public static WebElement btnUnFiltering;
	
	@FindBy(xpath="//table/tbody/tr/td[5]")
	public static WebElement titleEmail;
	
	@FindBy(xpath="//table/tbody/tr/td[2]")
	public static WebElement titleDate;
	
	@FindBy(xpath="//td[1]/input[@class='bulk-action-item']")
	public static WebElement checkboxCart;
	
	@FindBy(xpath="//*[@id='menu1']")
	public static WebElement btnChoose;
	
	@FindBy(xpath="//a[contains(.,'Gửi mail thông báo')]")
	public static WebElement titleSendMailIntification;
	
	@FindBy(xpath="//a[contains(.,'Gửi mail báo sắp hết hàng')]")
	public static WebElement titleInfomationPersistentCartOutOfStock;
	
	@FindBy(xpath="//span[@class='bog']/b")
	public static WebElement titleMailnotification;
	
	@FindBy(xpath="//*[@id='SubjectMailOne']")
	public static WebElement txtTitleMail;

	@FindBy(xpath="//a[contains(.,'Xóa')]")
	public static WebElement titleDelete;
	
	@FindBy(xpath="//button[contains(.,'Xóa')]")
	public static WebElement btnDelete;
	
	@FindBy(xpath="//*[@id='btnResetMailOne']")
	public static WebElement btnResetMailOne;
	
	@FindBy(xpath="//div[@class='cart-box']")
	public static WebElement titleCart;
	
	@FindBy(xpath="//iframe[@id='admin_bar_iframe']")
	public static WebElement iframeAdminBar;
	
	@FindBy(xpath="//*[@id='close-admin-bar']")
	public static WebElement iconCloseAdminBar;
	
	@FindBy(xpath="//tr[1]/td[6]/a[@title='Xóa']")
	public static WebElement iconDeleteProductAtCart;
	
	@FindBy(xpath="//a[@href='/account/logout']")
	public static WebElement btnLogout;
	
	@FindBy(xpath="//span[contains(.,'Tên sản phẩm')]")
	public static WebElement titleProducrAtCart;
	
	
}
