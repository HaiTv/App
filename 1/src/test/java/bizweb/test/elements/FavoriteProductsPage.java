package bizweb.test.elements;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class FavoriteProductsPage {
	
	@FindBy(xpath="//span[contains(.,'Sản phẩm yêu thích')]")
	public static WebElement titleApp;
	
	@FindBy(xpath="//a[contains(.,'Cấu hình')]")
	public static WebElement btnConfig;
	
	@FindBy(xpath="//div[1]/div/div/div[1]/div[1]/div/button")
	public static WebElement btnSetupThemePresent;
	
	@FindBy(xpath="//span[@class='title']")
	public static WebElement titleConfig;
	
	@FindBy(xpath="//span[contains(.,'Thêm vào yêu thích')]")
	public static WebElement btnAddFavorite;
	
	@FindBy(xpath="//span[contains(.,'Đã yêu thích')]")
	public static WebElement btnHasFavorite;
	
	@FindBy(xpath="//a[@class='iWishView']")
	public static WebElement btnWishList;
	
	@FindBy(xpath="//p[contains(.,'iPad Wifi 3G 16GB')]")
	public static WebElement titleProductAtWishList;
	
	@FindBy(xpath="//a[@href='/Wishlists/ProductWishlist']")
	public static WebElement btnManagerProduct;
	
	@FindBy(xpath="//td[contains(.,'iPad Wifi 3G 16GB')]")
	public static WebElement titleProductAtManagerProduct;
	
	
}
