package bizweb.test.elements;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CloudStoragePage {
	
	//Kết nối Google Drive
	@FindBy(xpath = "//a[contains(.,'Google')]")
	public static WebElement iconConnectAccount;
	
	@FindBy(xpath = "//a[contains(.,'Đăng nhập')]")
	public static WebElement btnLogin;
	
	@FindBy(xpath = "//input[@placeholder='Tìm kiếm']")
	public static WebElement txtSearch;
	
	@FindBy(xpath = "//thead/tr/th[1]")
	public static WebElement titleAccount;
	
	@FindBy(xpath = ".//*[@id='account-detail']/a")
	public static WebElement spanAccount;
	
	@FindBy(xpath = ".//*[@id='group-account']/div[2]/div[2]/a")
	public static WebElement btnDeleteAccount;
	
	@FindBy(xpath = "html/body/div[1]/div/div/div[3]/button[2]")
	public static WebElement btnAccept;
	
	@FindBy(xpath = "//a[contains(.,'Thư')]")
	public static WebElement btnCreateFolder;
	
	@FindBy(xpath = ".//*[@id='txtNewFolder']")
	public static WebElement txtNewFolder;
	
	@FindBy(xpath = "html/body/div[1]/div/div/div[3]/button[2]")
	public static WebElement btnCreateNewFolder;
	
	@FindBy(xpath = "//tr[1]/td[2]/a/span")
	public static WebElement titleNameFolder;
	
	@FindBy(xpath = "//tr[2]/td[2]/a/span")
	public static WebElement titleNameFolderTwo;
	
	@FindBy(xpath = "//*[@id='panel-account']/div[3]/table/tbody/tr/td[2]/a")
	public static WebElement titleNameFileAtFolder;
	
	@FindBy(xpath = "//*[@id='panel-account']/div[3]/table/tbody/tr[2]/td[2]/a")
	public static WebElement titleNameFile;
	
	@FindBy(xpath = "//*[@id='toast-container']/div/div[3]/div")
	public static WebElement titleErrorUploadFile;
	
	
	@FindBy(xpath = "//tr[1]/td[6]/a[1]")
	public static WebElement iconViewFolder;
	
	@FindBy(xpath = "//tr[2]/td[6]/a[1]")
	public static WebElement iconViewFile;
	
	@FindBy(xpath = "//tr[1]/td[6]/a[2]")
	public static WebElement iconLinkFolder;
	
	@FindBy(xpath = "//tr[2]/td[6]//a[@title='Nhận liên kết']")
	public static WebElement iconLinkFile;
	
	@FindBy(xpath = "html/body/div[1]/div/div/div[2]/div/span/button")
	public static WebElement btnCopy;
	
	@FindBy(xpath = "//div[2]/div[2]/div/div[1]")
	public static WebElement titleNameFolderAtGoogleDrivePage;
	
	@FindBy(xpath = "//div[@class='drive-viewer-toolstrip-name drive-viewer-rename-enabled']")
	public static WebElement titleNameFileAtGoogleDrivePage;
	
	@FindBy(xpath = "//*[@id='BreadcrumbTitle']")
	public static WebElement titleNameFileAtOneDrive;
	
	@FindBy(xpath = "//tr/th[1]/div/a")
	public static WebElement iconDelete;
	
	//Upload file
	@FindBy(xpath = "//span[@class='btn btn-default fileinput-button']")
	public static WebElement btnUploadfile;
	
	@FindBy(xpath = "//*[@id='browse-location']/div/div[2]/span/input")
	public static WebElement inputUpload;
	
	@FindBy(xpath = "//th[1]/div/a[1]")
	public static WebElement iconDownload;
	
	@FindBy(xpath = "//th[1]/div/a[2]")
	public static WebElement iconCopyFile;
	
	@FindBy(xpath = "html/body/div[1]/div/div/div[2]/div/div[1]/a/span")
	public static WebElement titleAccountContainsFile;
	
	@FindBy(xpath = "//div[2]/div/div[2]/div/div[1]/a")
	public static WebElement titleFolderContainsFile;
	
	@FindBy(xpath = "//div[2]/div/div[2]/div/div[2]/a")
	public static WebElement titleFolderTwoContainsFile;
	
	@FindBy(xpath = "//div[1]/h4/span[1]/a")
	public static WebElement BackFolderOrFile;
	
	
	@FindBy(xpath = "//th[1]/div/a[3]")
	public static WebElement iconMove;
	
	@FindBy(xpath = "//th[1]/div/a[4]")
	public static WebElement iconDeleteFile;
	
	//Kết nối Dropbox
	@FindBy(xpath = "//a[contains(.,'Dropbox')]")
	public static WebElement iconDropbox;
	
	@FindBy(xpath = "//input[@name='login_email']")
	public static WebElement txtEmailDropbox;
	
	@FindBy(xpath = "//input[@name='login_password']")
	public static WebElement txtPassDropbox;
	
	@FindBy(xpath = "//div[3]//button[contains(.,'Sign in')]")
	public static WebElement btnSigninDropbox;
	
	@FindBy(xpath = "//button[contains(.,'Allow')]")
	public static WebElement btnAllow;
	
	
	
	//Kết nối OneDrive
	
	@FindBy(xpath = "//iframe[contains(@id,'sdx_ow_iframe')]")
	public static WebElement iframeOneDrive;
	
	@FindBy(xpath = "//a[contains(.,'OneDrive')]")
	public static WebElement iconOneDrive;
	
	@FindBy(xpath = "//input[@name='loginfmt']")
	public static WebElement txtEmailOneDrive;
	
	@FindBy(xpath = "//input[@value='Next']")
	public static WebElement btnNext;
	
	@FindBy(xpath = "//input[@name='passwd']")
	public static WebElement txtPassOneDrive;
	
	@FindBy(xpath = "//input[@value='Sign in']")
	public static WebElement btnSigninOneDrive;
	
	@FindBy(xpath = "//span[@class='BreadcrumbBar-item']")
	public static WebElement titleNameFolderOneDrive;
	
	@FindBy(xpath = "//div[@class='dataTables_info ng-binding']")
	public static WebElement titleDontAccountConnect;
	
	
}

	
