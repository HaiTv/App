package bizweb.test.stepsdefine;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.support.PageFactory;

import bizweb.test.actions.Base;
import bizweb.test.elements.BizwebFormPage;
import bizweb.test.elements.PersistentCartPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class PersistentCart extends Base {

	String email = "haibiboy1995@gmail.com";

	Date today = new Date(System.currentTimeMillis());
	SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy");
	String date = timeFormat.format(today.getTime());

	public PersistentCart() {
		PageFactory.initElements(driver, PersistentCartPage.class);
		PageFactory.initElements(driver, BizwebFormPage.class);
	}

	@When("^I install persistent cart app success$")
	public void i_install_persistent_cart_app_success() throws Throwable {
		installApp("Lưu trữ giỏ hàng");
	}

	@Then("^I see the persistent cart app title displayed$")
	public void i_see_the_persistent_cart_app_title_displayed() throws Throwable {
		waitElementWithText(PersistentCartPage.titleApp, "Lưu trữ giỏ hàng");
	}

	@When("^I choose to turn off the fixed cart$")
	public void i_choose_to_turn_off_the_fixed_cart() throws Throwable {
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		selectDropbox(PersistentCartPage.selectboxActivated, "Tắt");
		// selectDropbox(PersistentCartPage.selectboxActivated, 1);
		exitIframe();
	}

	@When("^I click button save config$")
	public void i_click_button_save_config() throws Throwable {
		Thread.sleep(2000);
		waitElementToClick(PersistentCartPage.btnSave);
	}

	@Then("^I see message save config success$")
	public void i_see_message_save_config_success() throws Throwable {
		waitElementWithText(BizwebFormPage.titleNotification, "Cấu hình thành công");
	}

	@When("^I click put in the cart at product want add$")
	public void i_click_put_in_the_cart_at_product_want_add() throws Throwable {
		Thread.sleep(2000);
		clickToPoint(PersistentCartPage.buttonAddCart);
	}

	@Then("^I see product at cart$")
	public void i_see_product_at_cart() throws Throwable {
		waitElementWithText(PersistentCartPage.titleAddCartSuccess, "Bạn đã thêm iPad Wifi 3G 16GB vào giỏ hàng");
		closeTabWindow(2);
		switchTabWindow(1);
	}

	@When("^I am on List cart Page$")
	public void i_am_on_List_cart_Page() throws Throwable {
		waitElementToClick(PersistentCartPage.btnListCart);
	}

	@Then("^I do not see product at list cart$")
	public void i_do_not_see_product_at_list_cart() throws Throwable {
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		waitElementWithText(PersistentCartPage.titleDontProduct,
				"Không tìm thấy sản phẩm phù hợp với điều kiện tìm kiếm");
		exitIframe();
	}

	@Given("^I access config page$")
	public void i_access_config_page() throws Throwable {
		switchTabWindow(1);
		waitElementToClick(PersistentCartPage.btnConfig);
	}

	@When("^I choose to turn on the fixed cart$")
	public void i_choose_to_turn_on_the_fixed_cart() throws Throwable {
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		selectDropbox(PersistentCartPage.selectboxActivated, 0);
		exitIframe();
	}

	@When("^I turn off admin bar$")
	public void i_turn_off_admin_bar() throws Throwable {
		switchTabWindow(2);
		waitElement(PersistentCartPage.iframeAdminBar);
		switchToIframe(PersistentCartPage.iframeAdminBar);
		waitElementToClick(PersistentCartPage.iconCloseAdminBar);
		exitIframe();
	}

	@When("^I login account customer$")
	public void i_login_account_customer() throws Throwable {
		waitElementToClick(PersistentCartPage.titleLoginFrontend);
		waitElementToSendKeys(PersistentCartPage.txtEmailFrontend, email);
		waitElementToSendKeys(PersistentCartPage.txtPassFrontend, "haibib0y1995");
		waitElementToClick(PersistentCartPage.btnLoginFrontend);

	}

	@When("^I see product at list cart$")
	public void i_see_product_at_list_cart() throws Throwable {
		Thread.sleep(2000);
		refresh();
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		waitElementWithText(PersistentCartPage.titleProduct, "Họ tên");
	}

	@When("^I enter the wrong email customer$")
	public void i_enter_the_wrong_email_customer() throws Throwable {
		waitElementToSendKeys(PersistentCartPage.txtEmail, "haitranvan@gmail.com");
	}

	@When("^I click filtering$")
	public void i_click_filtering() throws Throwable {
		waitElementToClick(PersistentCartPage.btnFiltering);
	}

	@Then("^I do not see product at list cart when filtering$")
	public void i_do_not_see_product_at_list_cart_when_filtering() throws Throwable {
		waitElementWithText(PersistentCartPage.titleDontProduct,
				"Không tìm thấy sản phẩm phù hợp với điều kiện tìm kiếm");
	}

	@When("^I enter the correct email customer$")
	public void i_enter_the_correct_email_customer() throws Throwable {
		clear(PersistentCartPage.txtEmail);
		waitElementToSendKeys(PersistentCartPage.txtEmail, email);
	}

	@Then("^I see product at list cart contains email customer$")
	public void i_see_product_at_list_cart_contains_email_customer() throws Throwable {
		waitElementWithText(PersistentCartPage.titleEmail, email);
	}

	@When("^I enter the wrong date persistent$")
	public void i_enter_the_wrong_date_persistent() throws Throwable {
		clear(PersistentCartPage.txtEmail);
		waitElementToSendKeys(PersistentCartPage.txtDate, "12/08/2017");
	}

	@When("^I enter the correct date persistent$")
	public void i_enter_the_correct_date_persistent() throws Throwable {
		clear(PersistentCartPage.txtDate);
		waitElementToSendKeys(PersistentCartPage.txtDate, date);
	}

	@Then("^I see product at list cart contains date persistent$")
	public void i_see_product_at_list_cart_contains_date_persistent() throws Throwable {
		waitElementWithText(PersistentCartPage.titleDate, date);
	}

	@When("^I enter the correct email customer and date persistent$")
	public void i_enter_the_correct_email_customer_and_date_persistent() throws Throwable {
		clear(PersistentCartPage.txtEmail);
		clear(PersistentCartPage.txtDate);
		waitElementToSendKeys(PersistentCartPage.txtEmail, email);
		waitElementToSendKeys(PersistentCartPage.txtDate, date);
	}

	@Then("^I see product at list cart contains email customer and date persistent$")
	public void i_see_product_at_list_cart_contains_email_customer_and_date_persistent() throws Throwable {
		waitElementWithText(PersistentCartPage.titleDate, date);
		waitElementWithText(PersistentCartPage.titleEmail, email);
	}

	@When("^I click unfiltering$")
	public void i_click_unfiltering() throws Throwable {
		waitElementToClick(PersistentCartPage.btnUnFiltering);
	}

	@When("^I click title view detail$")
	public void i_click_title_view_detail() throws Throwable {
		waitElementToClick(PersistentCartPage.titleViewDetail);
	}

	@Then("^I see display infomation persistent cart$")
	public void i_see_display_infomation_persistent_cart() throws Throwable {
		waitElementToClick(PersistentCartPage.titleInfomationPersistentCart);
		waitElementToClick(PersistentCartPage.btnCloseViewDetail);
	}

	@When("^I click checkbox at cart want send$")
	public void i_click_checkbox_at_cart_want_send() throws Throwable {
		waitElement(PersistentCartPage.checkboxCart);
		clickToPoint(PersistentCartPage.checkboxCart);
		Thread.sleep(1000);
	}

	@When("^I click send mail notification out of stock$")
	public void i_click_send_mail_notification_out_of_stock() throws Throwable {
		waitElementToClick(PersistentCartPage.btnChoose);
		waitElementToClick(PersistentCartPage.titleInfomationPersistentCartOutOfStock);
		exitIframe();
	}

	@Then("^I see message send mail notification out of stock success$")
	public void i_see_message_send_mail_notification_out_of_stock_success() throws Throwable {
		Thread.sleep(3000);
		waitElementWithText(BizwebFormPage.titleNotification, "Gửi mail báo sắp hết hàng thành công!");
	}

	@Then("^I see mail notification out of stock dislay at email customer$")
	public void i_see_mail_notification_out_of_stock_dislay_at_email_customer() throws Throwable {
		openNewTabWindow("https://www.google.com/gmail/about/");
		waitElementToClick(BizwebFormPage.btnSigninGmail);
		loginGmail(email, "haibib0y1995");
		Thread.sleep(3000);
		waitElementWithText(PersistentCartPage.titleMailnotification,
				"Mail thông báo sản phẩm trong giỏ hàng của bạn trên shop ventomarme90");
		waitElementToClick(PersistentCartPage.titleMailnotification);
		waitElementToClick(BizwebFormPage.btnDeleteMail);
		Thread.sleep(2000);
		closeTabWindow(2);
	}

	@When("^I enter invalid title mail$")
	public void i_enter_invalid_title_mail() throws Throwable {
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		clear(PersistentCartPage.txtTitleMail);
		exitIframe();
	}

	@Then("^I see message error email$")
	public void i_see_message_error_email() throws Throwable {
		waitElementWithText(BizwebFormPage.titleNotification, "Dữ liệu không hợp lệ");
		Thread.sleep(5000);
	}

	@When("^I enter valid title mail$")
	public void i_enter_valid_title_mail() throws Throwable {
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		waitElementToSendKeys(PersistentCartPage.txtTitleMail, "Thông báo gửi Email");
		exitIframe();
	}

	@When("^I click checkbox at cart want send notification$")
	public void i_click_checkbox_at_cart_want_send_notification() throws Throwable {
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		waitElement(PersistentCartPage.checkboxCart);
		clickToPoint(PersistentCartPage.checkboxCart);
	}

	@When("^I click send mail notification$")
	public void i_click_send_mail_notification() throws Throwable {
		waitElementToClick(PersistentCartPage.btnChoose);
		waitElementToClick(PersistentCartPage.titleSendMailIntification);
		exitIframe();
	}

	@Then("^I see message send mail notification success$")
	public void i_see_message_send_mail_notification_success() throws Throwable {
		Thread.sleep(3000);
		waitElementWithText(BizwebFormPage.titleNotification, "Gửi mail thông báo thành công!");
	}

	@Then("^I see mail notification dislay at email customer$")
	public void i_see_mail_notification_dislay_at_email_customer() throws Throwable {
		openNewTabWindow("https://www.google.com/gmail/about/");
		waitElementToClick(BizwebFormPage.btnSigninGmail);
		waitElementToClick(BizwebFormPage.btnUserAnotherAccount);
		loginGmail(email, "haibib0y1995");
		Thread.sleep(3000);
		waitElementWithText(PersistentCartPage.titleMailnotification, "Thông báo gửi Email");
		waitElementToClick(PersistentCartPage.titleMailnotification);
		waitElementToClick(BizwebFormPage.btnDeleteMail);
		Thread.sleep(2000);
		closeTabWindow(2);
	}

	@When("^I click button Reset email default$")
	public void i_click_button_Reset_email_default() throws Throwable {
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		waitElement(PersistentCartPage.btnResetMailOne);
		clickToPoint(PersistentCartPage.btnResetMailOne);
		exitIframe();
	}

	@Then("^I see message notification reset email success$")
	public void i_see_message_notification_reset_email_success() throws Throwable {
		waitElementWithText(BizwebFormPage.titleNotification, "Cấu hình Email mặc định thành công.");
	}

	@When("^I logout account customer$")
	public void i_logout_account_customer() throws Throwable {
		switchTabWindow(2);
		Thread.sleep(2000);
		clickToPoint(PersistentCartPage.btnLogout);
		Thread.sleep(3000);
		closeTabWindow(2);
	}

	@When("^I perform delete product at cart$")
	public void i_perform_delete_product_at_cart() throws Throwable {
		switchTabWindow(1);
		openNewTabWindow("http://ventomarme90.bizwebvietnam.net/cart");
		waitElementToClick(PersistentCartPage.iconDeleteProductAtCart);
		Thread.sleep(2000);
	}

	@Then("^I see product display at cart$")
	public void i_see_product_display_at_cart() throws Throwable {
		closeTabWindow(2);
		switchTabWindow(1);
		openNewTabWindow("http://ventomarme90.bizwebvietnam.net/cart");
		waitElementWithText(PersistentCartPage.titleProducrAtCart, "Tên sản phẩm");
		closeTabWindow(2);
	}

	@When("^I click checkbox at cart want delete$")
	public void i_click_checkbox_at_cart_want_delete() throws Throwable {
		switchTabWindow(1);
		waitElementToClick(PersistentCartPage.btnListCart);
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		waitElement(PersistentCartPage.checkboxCart);
		clickToPoint(PersistentCartPage.checkboxCart);
	}

	@When("^I click title delete persistent cart$")
	public void i_click_title_delete_persistent_cart() throws Throwable {
		waitElementToClick(PersistentCartPage.btnChoose);
		Thread.sleep(1000);
		waitElementToClick(PersistentCartPage.titleDelete);
		Thread.sleep(1000);
	}

	@When("^I click button delete$")
	public void i_click_button_delete() throws Throwable {
		waitElementToClick(PersistentCartPage.btnDelete);
		exitIframe();
	}

	@Then("^I see display message delete success$")
	public void i_see_display_message_delete_success() throws Throwable {
		Thread.sleep(2000);
		waitElementWithText(BizwebFormPage.titleNotification, "Xóa thành công!");
	}

}
