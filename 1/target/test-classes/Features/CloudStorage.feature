#Feature: Cloud Storage
#I want to manage data from web to storage applications googledrive, dropbox and onedrive
#
#Scenario: Install app data success
#When I install app data success
#Then I see form connect in app page
#
#Scenario: Connect account googledrive
#When I connect account googledrive success
#Then I see account is connected
#
#Scenario: Create folder new success
#Given I access login account
#When I click button new folder
#And I enter valid name
#And I click button create
#Then I see message sucess display
#And I see folder have display
#
#Scenario: uploadfile unsuccess
#When I choose file invalid
#Then I see message uploadFile unsuccess
#
#Scenario: uploadfile success
#When I choose file valid
#Then I see message uploadFile success
#
#Scenario: View before file
#When I click view before file
#Then I see file at googledrive
#
#Scenario: Get link file
#When I click get link file
#
#Scenario: Search unsuccess while enter name file or folder invalid
#When I enter name file or folder invalid
#Then I dont see file or folder with name file or folder have enter
#
#Scenario: Search success while enter name file or folder valid 
#When I enter name file or folder valid
#Then I see file or folder with name file, folder enter
#
#Scenario: Download file success
#When I click file
#And I click icon download
#Then I see file have download
#
#Scenario: Move file success
#When I click icon move file
#And I choose folder contains file move
#Then I see message sucess display
#
#Scenario: Copy file Success
#Given I access to folder
#When I click file
#And I click icon copyfile
#And I choose folder contains file copy
#Then I see message sucess display
#
#Scenario: Delete file success
#When I click icon delete file
#And I click delete
#Then I see message sucess display
#
#Scenario: View before
#When I click view before
#Then I see folder at googledrive
#
#Scenario: Get link
#When I click get link
#
#Scenario: Delete folder Success
#When I click folder
#And I click icon delete
#And I click delete
#Then I see message sucess display
#
#Scenario: Delete account success
#When I perform logout success
#Then I dont see have account connect
#
#Scenario: Connect account onedrive
#When Connect account onedrive success
#Then I see account is connected
#
#Scenario: Create new folder success
#Given I access login account
#When I click button new folder
#And I enter valid name
#And I click button create
#Then I see message sucess display
#And I see folder have display
#
#Scenario: uploadfile unsuccess
#Scenario: uploadfile unsuccess
#When I choose file invalid
#Then I see message uploadFile unsuccess
#
#Scenario: uploadfile success
#When I choose file valid
#Then I see message uploadFile success
#
#Scenario: View before file
#When I click view before file
#Then I see file at onedrive
#
#Scenario: Get link file
#When I click get link file
#
#Scenario: Search unsuccess while enter invalid name file, folder
#When I enter name file or folder invalid
#Then I dont see file or folder with name file or folder have enter
#
#Scenario: Search success while enter valid name file, folder
#When I enter name file or folder valid
#Then I see file or folder with name file, folder enter
#
#Scenario: Download file success
#When I click file
#And I click icon download
#And I perform download
#Then I see file have download
#
#Scenario: Move file
#When I click icon move file
#And I choose folder contains file move 
#Then I see message sucess display
#
#Scenario: Copy file success
#Given I access to folder
#When I click file
#And I click icon copyfile
#And I choose folder contains file copy
#Then I see message sucess display
#
#Scenario: Delete file success
#When I click icon delete file
#And I click delete
#Then I see message sucess display
#
#Scenario: View before
#When I click view before
#Then I see folder at onedrive
#
#Scenario: Get link
#When I click get link
#
#Scenario: Delete folder success
#When I click folder
#And I click icon delete
#And I click delete
#Then I see message sucess display
#
#Scenario: Delete account Success
#When I perform logout success
#Then I dont see have account connect
 #
#Scenario: Connect account dropbox
#When Connect account dropbox success
#Then I see account is connected
#
#Scenario: Create new folder success
#Given I access login account 
#When I click button new folder
#And I enter valid name
#And I click button create
#Then I see message sucess display
#And I see folder have display account Dropbox
#
#Scenario: uploadfile unsuccess
#When I choose file invalid
#Then I see message uploadFile unsuccess
#
#Scenario: uploadfile success
#When I choose file valid
#Then I see message uploadFile success
#
#Scenario: Get link file
#When I click get link file
#
#Scenario: Search unsuccess while enter invalid name file, folder
#When I enter name file or folder invalid
#Then I dont see file or folder with name file or folder have enter
#
#Scenario: Search success while enter valid name file, folder
#When I enter name file or folder valid
#Then I see file or folder with name file, folder enter
#
#Scenario: Download File
#When I click file
#And I click icon download
#Then I see file have download
#
#Scenario: Move file success
#When I click icon move file
#And I choose folder contains file move 
#Then I see message sucess display
#
#Scenario: Create folder new two unsuccess
#When I click button folder new two 
#And I enter already exists name  
#And I click button create
#Then I see message error already exists name display
#
#Scenario: Create folder new two success
#When I click button folder new two 
#And I enter valid name two
#And I click button create
#Then I see message sucess display
#And I see folder have display two
#
#Scenario: Copy file success
#Given I access to folder at dropbox
#When I click file
#And I click icon copyfile
#And I choose folder file two
#Then I see message sucess display
#And I back to folder or file
#And I see file at folder two
#
#Scenario: Delete file success
#When I click icon delete file at folder two
#And I click delete
#Then I see message sucess display
#
#Scenario: Delete file two success
#When I back to folder or file
#And I click icon delete folder at folder two
#And I click icon delete
#And I click delete
#Then I see message sucess display
#
#Scenario: Delete folder one success
#When I click folder at dropbox
#And I click icon delete
#And I click delete
#Then I see message sucess display
#
#Scenario: Delete account success
#When I perform logout success
#Then I dont see have account connect
#
#Scenario: Remove app success
#When I remove app success
#Then I do not see settings page of app